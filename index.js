let current_date = new Date();
function CreateNewUser(){
    let FirstName  = String(prompt("Name: "));
    let SurName  = String(prompt("Surname"));
    let bDay = prompt("Birthday (dd.mm.yyyy): ").split('.');
    return new Object({
        firstName: FirstName,
        lastName: SurName,
        birthday: new Date(bDay[2],bDay[1],bDay[0]),
        getLogin(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge(){
            return current_date.getFullYear() - this.birthday.getFullYear();
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + ' ' + this.birthday.getDate() + '.' + this.birthday.getMonth() + '.' + this.birthday.getFullYear();
        }
    })
}
let user = CreateNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());